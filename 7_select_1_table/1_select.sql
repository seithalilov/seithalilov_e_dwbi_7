﻿use [labor_sql];
go

--1
SELECT maker,[type]
FROM product
WHERE [type] = 'Laptop'
ORDER BY maker 

--2
SELECT model,ram,screen,price
FROM laptop
WHERE price > 1000
ORDER BY ram ASC, price DESC

--3
SELECT *
FROM printer
WHERE color = 'y'
ORDER BY price DESC

--4
SELECT model,speed,hd,cd,price
FROM pc
WHERE cd = '12x' OR cd = '24x'
ORDER BY speed DESC

--5
SELECT [name],class
FROM ships
WHERE [name] = class
ORDER BY name

--6
SELECT *
FROM pc
WHERE speed >= 500 AND price < 800
ORDER BY price DESC

--7
SELECT *
FROM printer
WHERE type <> 'Matrix' AND price < 300
ORDER BY type DESC

--8
SELECT model,speed
FROM pc
WHERE price BETWEEN 400 and 600
ORDER BY  hd 

--9
SELECT model,speed,hd,price
FROM laptop
WHERE screen >= 12
ORDER BY price DESC

--10
SELECT model,type,price
FROM printer
WHERE price <300
ORDER BY type DESC

--11
SELECT model,ram,price
FROM laptop
WHERE ram =64
ORDER BY screen 

--12
SELECT model,ram,price
FROM pc
WHERE ram > 64
ORDER BY hd

--13
SELECT model,speed,price
FROM pc
WHERE speed BETWEEN 500 AND 750
ORDER BY hd DESC

--14
SELECT *
FROM outcome_o
WHERE out >2000
ORDER BY date DESC
--15
SELECT *
FROM income_o
WHERE inc BETWEEN 5000 AND 10000
ORDER BY inc

--16
SELECT *
FROM Income
WHERE point = 1
ORDER BY inc 

--17
SELECT *
FROM outcome
WHERE point = 2
ORDER BY out 

--18
SELECT *
FROM classes
WHERE country = 'Japan'
ORDER BY type DESC

--19
SELECT name,launched
FROM ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC

--20
SELECT ship,battle,result
FROM outcomes
WHERE battle = 'Guadalcanal' and result <> 'sunk'
ORDER BY ship DESC

--21
SELECT ship,battle,result
FROM outcomes
WHERE result = 'sunk'
ORDER BY ship

--22
SELECT class,displacement
FROM classes
WHERE displacement >=40000
ORDER BY type

--23
SELECT trip_no,town_from,town_to
FROM trip
WHERE town_from = 'London' OR town_to = 'London'
ORDER BY time_out

--24
SELECT trip_no,plane,town_from,town_to
FROM trip
WHERE plane = 'TU-134'
ORDER BY time_out DESC

--25
SELECT trip_no,plane,town_from,town_to
FROM trip
WHERE plane <> 'IL-86'
ORDER BY plane

--26
SELECT trip_no,town_from,town_to
FROM trip
WHERE town_from <>'Rostov' AND town_to <>'Rostov'
ORDER BY plane 

--27
SELECT model
FROM pc
WHERE model LIKE '%1%1%'

--28
SELECT *
FROM outcome
WHERE MONTH([date]) = 3

--29
SELECT *
FROM outcome_o
WHERE DAY([date]) = 14

--30
SELECT name
FROM ships
WHERE name LIKE 'W%n'

--31
SELECT name
FROM ships
WHERE name LIKE '%e%e%'

--32
SELECT name,launched
FROM ships
WHERE name NOT LIKE '%a'

--33
SELECT name 
FROM battles
WHERE name like '% %' AND name NOT LIKE '%c'

--34
SELECT *
FROM trip
WHERE DATEPART (hour,time_out) >= 12 AND DATEPART (hour,time_out) <= 17

--35
SELECT *
FROM trip
WHERE DATEPART (hour,time_in) >= 17 AND DATEPART (hour,time_in) <= 23 

--36
SELECT *
FROM trip
WHERE DATEPART (hour,time_out) >= 21 OR DATEPART (hour,time_out) <= 10

--37
SELECT DISTINCT date
FROM pass_in_trip
WHERE place LIKE '1_'

--38
SELECT DISTINCT date
FROM pass_in_trip
WHERE place LIKE '%c'

--39
SELECT  SUBSTRING([name], CHARINDEX(' ', [name])+1, 100) as [Surname]
FROM passenger
WHERE name LIKE '% S%'

--40
SELECT  SUBSTRING([name], CHARINDEX(' ', [name])+1, 100) as [Surname]
FROM passenger
WHERE name NOT LIKE '% J%'

--41
SELECT  CONCAT('avarage price = ', AVG(price)) price 
FROM laptop

--42
SELECT CONCAT('unique code: ',code),
		CONCAT('model: ', model),
		CONCAT('CPU speed: ',speed, ' Mhz'),
		CONCAT('RAM: ',ram, ' mb'),
		CONCAT('HD size: ',hd, ' gb'),
		CONCAT('CD speed: ',cd),
		CONCAT('price: ',price,'$')
FROM pc

--43
SELECT CONVERT(varchar, [date],102) date
FROM income

--44
DECLARE @result varchar(50)
SELECT ship, battle, result = 
	CASE result
			WHEN 'OK' THEN 'цілий'
			WHEN 'damaged' THEN 'пошкоджений'
			WHEN 'sunk' THEN 'потоплений'
	END
FROM outcomes

--45
SELECT CONCAT('row:', LEFT(place,1)) [row], CONCAT('seat:', RIGHT(TRIM(place),1)) [seat]
FROM pass_in_trip

--46
SELECT trip_no,id_comp,plane,time_out,time_in,CONCAT('from ',TRIM(town_from),' to ',TRIM(town_to)) [route]
FROM trip

--47
SELECT CONCAT(left(trip_no, 1),
				right(trip_no,1),
				left(id_comp, 1),
				right(id_comp,1),
				left(plane, 1),
				right(TRIM(plane),1),
				left(town_from, 1),
				right(TRIM(town_from),1),
				left(town_to, 1),
				right(TRIM(town_to),1),
				left(time_out,1),
				right(time_out,1),
				left(time_in, 1),
				right(time_in,1)
				) 
FROM trip

--48
SELECT maker, COUNT(model) model_count
FROM product
GROUP BY maker
HAVING COUNT(model) >=2

--49
SELECT city, COUNT(*) as [count]
FROM (SELECT town_from city FROM trip UNION ALL SELECT town_to city FROM trip) TEMP
GROUP BY city

--50
SELECT [type] , COUNT(model) [count]
FROM printer
GROUP BY [type]

--51
SELECT cd, COUNT(model) [count]
FROM pc
GROUP BY cd

--52
SELECT trip_no,
	CONVERT(time, time_in - time_out)
FROM trip

--53
SELECT coalesce(cast(point as varchar(20)),'All points')  point,
		coalesce(cast([date]as varchar(20)), 'Total') [date],
		sum(out) [sum],
		(SELECT MAX(out) FROM outcome WHERE point = outcome.point) [max],
		(SELECT MIN(out) FROM outcome WHERE point = outcome.point) [min]
FROM outcome
GROUP BY point, [date] WITH ROLLUP

--54
SELECT trip_no,(LEFT (place,1)) [row],COUNT(*) [count]
FROM pass_in_trip
GROUP BY trip_no,left(place,1)

--55
SELECT COUNT(name) [count]
FROM passenger
WHERE name LIKE '% [SBA]%'
