use [labor_sql];
go

--1
SELECT maker,type,speed,hd
FROM product JOIN pc  ON pc.model = product.model
WHERE hd <=8

--2
SELECT DISTINCT(maker)
FROM product JOIN pc ON pc.model = product.model 
WHERE speed <= 500

--3
SELECT DISTINCT(maker)
FROM product JOIN laptop ON laptop.model = product.model 
WHERE speed <= 500

--4
SELECT l1.model,l2.model, l1.hd, l1.ram
FROM laptop l1 , laptop l2
WHERE l1.model>l2.model AND l1.ram = l2.ram AND l1.hd = l2.hd

--5
SELECT c1.country,c1.class, c2.class 
FROM classes c1,classes c2
WHERE c1.country = c2.country AND c1.type = 'bc' and c2.type = 'bb'

--6
SELECT distinct pc.model,maker
FROM pc join product on product.model = pc.model
WHERE price < 600

--7
SELECT distinct printer.model,maker
FROM printer join product on product.model = printer.model
WHERE price > 300

--8
SELECT pc.model,pc.price,maker
FROM product  JOIN pc on product.model = pc.model
			  

--9
SELECT pc.model,price,maker
FROM pc JOIN product ON product.model = pc.model


--10
SELECT maker, type,product.model,speed
FROM product JOIN laptop ON product.model= laptop.model
WHERE  speed > 600

--11
SELECT name, displacement
FROM ships s LEFT JOIN classes c ON s.class = c.class 

--12
SELECT name, date
FROM battles b JOIN outcomes o ON o.battle = b.name
WHERE o.result <> 'sunk'

--13
SELECT name,country
FROM ships s LEFT JOIN classes c ON c.class = s.class

--14
SELECT distinct name
FROM company c JOIN trip t ON t.id_comp = c.id_comp
WHERE plane = 'Boeing'

--15
SELECT name,date
FROM passenger s RIGHT JOIN pass_in_trip p ON p.id_psg = s.id_psg

--16
SELECT pc.model,speed,hd
FROM pc JOIN product ON product.model = pc.model
WHERE maker = 'A' AND (hd = 10 or hd = 20)

--17
SELECT maker, pc,laptop,printer
FROM product
PIVOT (COUNT(model) FOR type IN (pc,laptop,printer)) as product_pivot

--18
SELECT 'average price' [avg_],[11],[12],[14],[15]
FROM (SELECT screen,price FROM laptop) as info
PIVOT (AVG(price) FOR screen IN ([11],[12],[14],[15]))as laptop_pivot

--19
SELECT model,maker
FROM laptop l
CROSS APPLY (SELECT maker FROM product p WHERE p.model = l.model) y

--20
SELECT code,l.model,speed,ram,hd,price,screen, max_price
FROM laptop l
CROSS APPLY (SELECT MAX(price) as max_price 
			FROM laptop l2 JOIN product p ON p.model = l2.model
			WHERE p.maker = (SELECT maker FROM product p2 WHERE p2.model = l.model)) info

--21
--22
--23
--24