
create trigger medicine_zone_TR ON medicine_zone
AFTER INSERT,UPDATE
AS
IF ((select medicine_id from inserted)  not in (select id from medicine)) OR ((select zone_id from inserted)  not in (select id from zone))
		ROLLBACK TRAN
		PRINT 'FOREIGN KEY ERROR';
GO

create trigger pharmacy_medicine_TR ON pharmacy_medicine
AFTER INSERT,UPDATE
AS
IF ((select pharmacy_id from inserted)  not in (select id from pharmacy)) OR ((select medicine_id from inserted)  not in (select id from medicine))
		ROLLBACK TRAN
		PRINT 'FOREIGN KEY ERROR';
GO

create trigger employee_pharmacy_TR ON employee
AFTER INSERT,UPDATE
AS
IF ((select pharmacy_id from inserted)  not in (select id from pharmacy))
		ROLLBACK TRAN
		PRINT 'FOREIGN KEY ERROR';
IF ((select post from inserted) not in (select post from post))
		ROLLBACK TRAN
		PRINT 'FOREIGN KEY ERROR';
GO

create trigger pharmacy_street_TR ON pharmacy
AFTER INSERT,UPDATE
AS
IF ((select street from inserted)  not in (select street from street))
		ROLLBACK TRAN
		PRINT 'FOREIGN KEY ERROR';
Go

CREATE TRIGGER post_TR ON post
AFTER UPDATE,DELETE
AS
ROLLBACK TRAN;
GO

CREATE TRIGGER street_TR ON street
AFTER UPDATE
AS
UPDATE pharmacy
SET street = (SELECT street FROM inserted)
WHERE street = (SELECT street FROM deleted)
GO

CREATE TRIGGER delete_street_TR ON street
AFTER DELETE
AS
UPDATE pharmacy
SET street = NULL
WHERE street = (SELECT street FROM deleted)
GO

CREATE TRIGGER zone_delete_TR ON [zone]
AFTER DELETE
AS
DELETE medicine_zone
WHERE zone_id = (SELECT id FROM deleted)
GO 

CREATE TRIGGER pharmacy_delete_TR ON pharmacy
AFTER DELETE
AS
UPDATE pharmacy_medicine
SET pharmacy_id=NULL
WHERE pharmacy_id = (SELECT id FROM deleted)
GO

CREATE TRIGGER medicine_delete_TR ON medicine
AFTER DELETE
AS
ROLLBACK TRAN;
GO

CREATE TRIGGER identity_number_TR ON employee
AFTER INSERT,UPDATE
AS
IF ((select identity_number from inserted) LIKE '%00')
	rollback tran
	print 'identity number cant end with 00';
go

CREATE TRIGGER ministry_code_TR ON medicine
AFTER INSERT,UPDATE
AS
IF ((SELECT ministry_code FROM inserted)NOT LIKE '[a-z]%')
	ROLLBACK TRAN
IF ((SELECT ministry_code FROM inserted)NOT LIKE '_[a-z]%')
	ROLLBACK TRAN
IF ((SELECT ministry_code FROM inserted) LIKE '[mp]%')
	ROLLBACK TRAN
IF ((SELECT ministry_code FROM inserted) LIKE '_[mp]%')
	ROLLBACK TRAN
IF ((SELECT ministry_code FROM inserted) NOT LIKE '__[-]%')
	ROLLBACK TRAN
IF ((SELECT ministry_code FROM inserted) NOT LIKE '___[0-9]%')
	ROLLBACK TRAN
IF ((SELECT ministry_code FROM inserted) NOT LIKE '____[0-9]%')
	ROLLBACK TRAN
IF ((SELECT ministry_code FROM inserted) NOT LIKE '_____[0-9]%')
	ROLLBACK TRAN
	IF ((SELECT ministry_code FROM inserted) NOT LIKE '______[-]%')
	ROLLBACK TRAN
	IF ((SELECT ministry_code FROM inserted) NOT LIKE '_______[0-9]%')
	ROLLBACK TRAN
	IF ((SELECT ministry_code FROM inserted) NOT LIKE '________[0-9]')
	ROLLBACK TRAN
GO

DROP TRIGGER ministry_code_TR
go
