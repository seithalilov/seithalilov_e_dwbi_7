
SELECT * FROM Authors
SELECT * FROM Authors_log
SELECT * FROM Books
SELECT * FROM BooksAuthors
SELECT * FROM Publishers

alter table Authors Add
	merge_no int IDENTITY(1,1);

ALTER TABLE Authors ADD 
	birthday DATE NULL,
	book_amount int not null default (0) check (book_amount >= 0),
	issue_amount int not null default (0) check (issue_amount>= 0),
	total_edition int not  null default (0) check (total_edition >= 0);

ALTER TABLE Authors 
DROP column book_amount, issue_amount,total_edition;
ALTER TABLE Books ADD 
	title varchar(30) not NULL DEFAULT 'Title',
	edition int not null default (1) check (edition >= 1),
	published date null,
	issue int null;

ALTER TABLE Publishers ADD 
	created date not NULL DEFAULT ('1900-01-01'),
	country varchar(20) not null default ('USA'),
	city	varchar(20) not null default ('NY'),
	book_amount int not null default (0) check (book_amount >= 0),
	issue_amount int not null default (0) check (issue_amount>= 0),
	total_edition int not  null default (0) check (total_edition >= 0);

ALTER TABLE Authors_log ADD 
	book_amount_old int  NULL,
	issue_amount_old int NULL,
	total_edition_old int NULL,
	book_amount_new int  NULL,
	issue_amount_new int NULL,
	total_edition_new int NULL;

drop trigger if exists updated_authors_TR
go 

