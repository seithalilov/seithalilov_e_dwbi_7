drop trigger if exists updated_authors_TR
go 

CREATE TRIGGER updated_authors_TR ON dbo.Authors
AFTER UPDATE
AS
UPDATE Authors
SET updated =GETDATE(), updated_by = SYSTEM_USER
INSERT INTO Authors_log
(author_id_new,name_new,[url_new],author_id_old,name_old,url_old,operation_type, operation_datetime,book_amount_old,issue_amount_old,total_edition_old,book_amount_new,issue_amount_new,total_edition_new)
SELECT inserted.id,inserted.name,inserted.url,deleted.id,deleted.name,deleted.url,'U',getdate(), deleted.book_amount, deleted.issue_amount, deleted.total_edition, inserted.book_amount, inserted.issue_amount, inserted.total_edition
from inserted
JOIN deleted ON inserted.merge_no = deleted.merge_no

ALTER TRIGGER  insert_authors_TR ON Authors
AFTER INSERT
AS
INSERT INTO Authors_log
(author_id_new,name_new,[url_new],operation_type, operation_datetime,book_amount_new,issue_amount_new,total_edition_new)
SELECT id,[name],[url],'I',getdate(),inserted.book_amount, inserted.issue_amount, inserted.total_edition
from inserted

ALTER TRIGGER delete_authors_TR ON dbo.Authors
AFTER DELETE
AS
INSERT INTO Authors_log
(author_id_old,name_old,[url_old],operation_type, operation_datetime,book_amount_old,issue_amount_old,total_edition_old)
SELECT id,[name],[url],'D',getdate(), deleted.book_amount, deleted.issue_amount, deleted.total_edition
from deleted

EXEC sp_configure 'show advanced options', 1;  
GO  
RECONFIGURE ;  
GO  
EXEC sp_configure 'nested triggers', 0 ;  
GO  
RECONFIGURE;  
GO  