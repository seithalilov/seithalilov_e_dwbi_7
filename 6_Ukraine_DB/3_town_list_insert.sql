use [people_ua_db]
go

drop table if exists [town_list]
go
create table [town_list](
[id]		int			identity	Primary Key,
[town]	nvarchar(20) not null,
)
go
insert into [town_list]  ([town])
values 
('Vinnytsa'),
('Volyn'),
('Dnepro'),
('Donetsk'),
('Jytomyr'),
('Zakarpattya'),
('Zaporijya'),
('Ivano-Frankivsk'),
('Kiev'),
('Kirovohrad'),
('Crimea'),
('Lugansk'),
('Lviv'),
('Nikolaev'),
('Odessa'),
('Poltava'),
('Rovno'),
('Summi'),
('Ternopyl'),
('Kharkiv'),
('Kherson'),
('Khmelnytsk'),
('Cherkassy'),
('Chernihov'),
('Chernovtsy')