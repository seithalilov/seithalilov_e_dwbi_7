USE anoDB
GO

BEGIN TRAN
UPDATE table_1
SET value_1 = 10
WHERE id = 1;

WAITFOR DELAY '00:00:05';

ROLLBACK;

SELECT value_1 FROM table_1 WHERE id = 1;
