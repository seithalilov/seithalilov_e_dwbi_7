ALTER TABLE dbo.cash_office 
ADD CONSTRAINT sell_date_def DEFAULT GETDATE() FOR sell_time
GO

ALTER TABLE dbo.cash_office 
ADD CONSTRAINT inserted_date_def DEFAULT GETDATE() FOR inserted_date
GO

ALTER TABLE dbo.product 
ADD CONSTRAINT amount_ck CHECK (amount > 0)
GO

ALTER TABLE dbo.product 
ADD CONSTRAINT price_ck CHECK (price > 0)
GO

ALTER TABLE dbo.cash_office 
ADD CONSTRAINT discount_def DEFAULT (0) FOR discount
GO

ALTER TABLE dbo.product 
ADD CONSTRAINT passcode_uq UNIQUE (passcode)
GO

ALTER TABLE dbo.cash_office 
ADD CONSTRAINT FK_product_id FOREIGN KEY (product_id) REFERENCES product(id)
ON DELETE NO ACTION
ON UPDATE CASCADE
GO
