--************************check GETDATE() on sell_date and inserted_date
insert into cash_office			(sell_time,
								amount,
								price,
								discount,
								total_price,
								product_id,
								cashier)

					values(		'2018-07-17',
								3,
								10,
								0,
								30,
								1,
								'Melnik');

--************************check amount>0 for amount
insert into cash_office			(sell_time,
								amount,
								price,
								discount,
								total_price,
								product_id,
								cashier)

					values(		'2018-07-17',
								-5,
								10,
								0,
								50,
								2,
								'Melnik');

--************************check price>0 for price
insert into cash_office			(sell_time,
								amount,
								price,
								discount,
								total_price,
								product_id,
								cashier)

					values(		'2018-07-17',
								5,
								-10,
								0,
								50,
								2,
								'Melnik');

--************************check default 0 for discount
insert into cash_office			(sell_time,
								amount,
								price,
								total_price,
								product_id,
								cashier)

					values(		'2018-07-17',
								5,
								10,
								50,
								2,
								'Melnik');

--************************check unique passcode
insert into product (			passcode,
								[name],
								[type],
								amount,
								price,
								expiry_date,
								man_date,
								income_price,
								provider)

					values(		420343024051,
								'chicken',
								'meat',
								20,
								88,
								'2018-07-23',
								'2018-07-15',
								60,
								'LvivMeat');

insert into product (			passcode,
								[name],
								[type],
								amount,
								price,
								expiry_date,
								man_date,
								income_price,
								provider)

					values(		420343024435,
								'chicken',
								'meat',
								20,
								88,
								'2018-07-23',
								'2018-07-15',
								60,
								'LvivMeat');

--************************check foreign key for cash_office
insert into cash_office			(sell_time,
								amount,
								price,
								total_price,
								product_id,
								cashier)

					values(		'2018-07-17',
								5,
								10,
								50,
								22,
								'Melnik');

select *
from cash_office

select *
from product