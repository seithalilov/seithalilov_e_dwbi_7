--************************check insert trigger
insert into cars				(model,
								brand,
								amount,
								type,
								man_date,
								price,
								factory_code,
								provider,
								engine_capacity,
								horse_power ,
								transmission,
								mileage)

					values(		'supra',
								'toyota',
								1,
								'coupe',
								'2018-01-01',
								35.500,
								'23435256467',
								'ToyotaMotors',
								3.2,
								678,
								'auto',
								0);

SELECT *
FROM cars
SELECT *
FROM cars_audit

--************************check update trigger
UPDATE cars
SET price = 35200
WHERE id = 1

--************************check delete trigger
DELETE cars
WHERE id = 1

