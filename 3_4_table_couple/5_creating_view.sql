CREATE VIEW products_sold_view
AS
SELECT c.sell_time, c.amount , c.total_price , p.passcode , p.[type], p.[name], p.income_price
FROM cash_office c
JOIN product p ON c.product_id = p.id
GO

DROP VIEW products_sold_view
GO

CREATE VIEW expiry_view
AS
SELECT  p.passcode , p.[type], p.[name], p.man_date, p.expiry_date, GETDATE() as 'current_date'
FROM  product p
WHERE [expiry_date] IS NOT NULL;
GO

DROP VIEW expiry_view
GO

CREATE VIEW audit_view
AS
SELECT  c.cashier , c.sell_time,c.amount,c.price,c.discount,c.product_id,c.total_price
FROM  cash_office c
WHERE cashier = 'Melnik'
WITH CHECK OPTION
GO