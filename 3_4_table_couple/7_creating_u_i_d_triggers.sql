CREATE TRIGGER updated_time_TR ON cars
AFTER UPDATE
AS
BEGIN
   INSERT INTO dbo.cars_audit 
   (model, brand, amount, type, man_date, price,factory_code, provider,engine_capacity,horse_power,transmission,mileage,procedure_type,procedure_time)    
   SELECT model, brand, amount, type, man_date, price,factory_code, provider,engine_capacity,horse_power,transmission,mileage,'U',GETDATE()
   FROM deleted;
END

CREATE TRIGGER inserted_time_TR ON cars
AFTER INSERT
AS
BEGIN
   INSERT INTO dbo.cars_audit 
   (model, brand, amount, type, man_date, price,factory_code, provider,engine_capacity,horse_power,transmission,mileage,procedure_type,procedure_time)    
   SELECT model, brand, amount, type, man_date, price,factory_code, provider,engine_capacity,horse_power,transmission,mileage,'I',GETDATE()
   FROM inserted;
END

CREATE TRIGGER deleted_time_TR ON cars
AFTER DELETE
AS
BEGIN
   INSERT INTO dbo.cars_audit 
   (model, brand, amount, type, man_date, price,factory_code, provider,engine_capacity,horse_power,transmission,mileage,procedure_type,procedure_time)    
   SELECT model, brand, amount, type, man_date, price,factory_code, provider,engine_capacity,horse_power,transmission,mileage,'D',GETDATE()
   FROM deleted;
END