CREATE TABLE [dbo].[cars]
(
 [id]            INT IDENTITY ,
 [model]		 VARCHAR(20) NOT NULL ,
 [brand]         VARCHAR(20) NOT NULL ,
 [amount]        INT NOT NULL ,
 [type]			 VARCHAR(10) not null,
 [man_date]      DATE NOT NULL ,
 [price]		 DECIMAL(10,2) NOT NULL ,
 [factory_code]  VARCHAR(20) NOT NULL ,
 [provider]		 VARCHAR(20) NOT NULL ,
 [engine_capacity]    decimal(5,2) NOT NULL ,
 [horse_power]    INT NOT NULL ,
 [transmission]	 VARCHAR(10) not null,
 [mileage]		 int not null,
 CONSTRAINT [PK_cars] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO

CREATE TABLE [dbo].[cars_audit]
(
 [id]            INT IDENTITY ,
 [model]		 VARCHAR(20) NOT NULL ,
 [brand]         VARCHAR(20) NOT NULL ,
 [amount]        INT NOT NULL ,
 [type]			 VARCHAR(10) not null,
 [man_date]      DATE NOT NULL ,
 [price]		 DECIMAL(10,2) NOT NULL ,
 [factory_code]  VARCHAR(20) NOT NULL ,
 [provider]		 VARCHAR(20) NOT NULL ,
 [engine_capacity]    decimal(5,2) NOT NULL ,
 [horse_power]    INT NOT NULL ,
 [transmission]	 VARCHAR(10) not null,
 [mileage]		 int not null,
 [procedure_type] varchar(1) not null,
 [procedure_time] datetime not null
 CONSTRAINT [PK_cars_audit] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO