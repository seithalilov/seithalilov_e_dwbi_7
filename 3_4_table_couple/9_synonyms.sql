CREATE SCHEMA E_Seithalilov
GO
CREATE SYNONYM cars_syn for E_S_module_3.dbo.cars
go
CREATE SYNONYM cars_audit_syn for E_S_module_3.dbo.cars_audit
go

SELECT *
FROM cars_syn

SELECT *
FROM cars_audit_syn

SELECT id,model,type
FROM cars_syn

SELECT *
FROM cars_audit_syn
WHERE procedure_type = 'D'

drop synonym cars_audit_syn
go
drop synonym cars_syn
go

SELECT *  FROM sys.synonyms
