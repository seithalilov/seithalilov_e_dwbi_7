
--************************************** [dbo].[product]

CREATE TABLE [dbo].[product]
(
 [id]			INT IDENTITY ,
 [passcode]     VARCHAR(12) NOT NULL ,
 [name]         VARCHAR(20) NOT NULL ,
 [type]         VARCHAR(20) NOT NULL ,
 [amount]       INT NOT NULL ,
 [price]        DECIMAL(5,2) NOT NULL ,
 [expiry_date]  DATE NULL ,
 [man_date]     DATETIME NOT NULL ,
 [income_price] DECIMAL(5,2) NOT NULL ,
 [provider]     VARCHAR(20) NOT NULL ,

 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [dbo].[cash_office]

CREATE TABLE [dbo].[cash_office]
(
 [id]            INT IDENTITY ,
 [sell_time]     DATETIME NOT NULL ,
 [amount]        INT NOT NULL ,
 [price]         DECIMAL(10,2) NOT NULL ,
 [discount]      INT NOT NULL ,
 [total_price]   DECIMAL(10,2) NOT NULL ,
 [inserted_date] DATETIME NOT NULL ,
 [updated_date]  DATETIME NOT NULL ,
 [product_id]    INT NOT NULL ,
 [cashier]       VARCHAR(10) NOT NULL ,

 CONSTRAINT [PK_sell] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO


drop table dbo.cash_office
go
drop table dbo.product
go