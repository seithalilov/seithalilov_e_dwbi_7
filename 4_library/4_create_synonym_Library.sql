CREATE DATABASE E_S_Library_synonym
GO

DROP DATABASE E_S_Library_synonym
GO

CREATE SYNONYM authors_syn for E_S_Library.dbo.authors
go

CREATE SYNONYM authors_log_syn for E_S_Library.dbo.authors_log
go

CREATE SYNONYM books_syn for E_S_Library.dbo.books
go

CREATE SYNONYM booksauthors_syn for E_S_Library.dbo.booksauthors
go

CREATE SYNONYM publishers_syn for E_S_Library.dbo.publishers
go
select *
from authors_log_syn
go