CREATE DATABASE E_S_Library_view
GO

DROP DATABASE E_S_Library_view
GO

CREATE VIEW authors_view
AS
SELECT *
FROM E_S_Library.dbo.Authors
GO

CREATE VIEW authors_log_view
AS
SELECT *
FROM E_S_Library.dbo.authors_log
GO

CREATE VIEW books_view
AS
SELECT *
FROM E_S_Library.dbo.Books
GO

CREATE VIEW booksauthors_view
AS
SELECT *
FROM E_S_Library.dbo.BooksAuthors
GO

CREATE VIEW publishers_view
AS
SELECT *
FROM E_S_Library.dbo.Publishers
GO