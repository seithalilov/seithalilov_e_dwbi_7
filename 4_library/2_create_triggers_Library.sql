ALTER TABLE dbo.Authors
ADD CONSTRAINT url_def DEFAULT 'www.authors_name.com' FOR [url]
GO

ALTER TABLE dbo.Authors
ADD CONSTRAINT inserted_by_def DEFAULT SYSTEM_USER FOR [inserted_by]
GO

ALTER TABLE dbo.Books
ADD CONSTRAINT inserted_by__books_def DEFAULT SYSTEM_USER FOR [inserted_by]
GO

ALTER TABLE dbo.Publishers
ADD CONSTRAINT inserted_by_publishers_def DEFAULT SYSTEM_USER FOR [inserted_by]
GO
ALTER TABLE dbo.BooksAuthors
ADD CONSTRAINT inserted_by_booksauthors_def DEFAULT SYSTEM_USER FOR [inserted_by]
GO

ALTER TABLE dbo.Publishers
ADD CONSTRAINT name_publishers_UK UNIQUE ([name])
GO

ALTER TABLE dbo.Publishers
ADD CONSTRAINT url_publishers_def DEFAULT 'www.publishers_name.com' FOR [url]
GO

ALTER TABLE dbo.Books
ADD CONSTRAINT url_books_UK UNIQUE ([url])
GO

CREATE TRIGGER updated_books_TR ON dbo.Books
AFTER UPDATE
AS
UPDATE Books
SET updated =GETDATE(), updated_by = SYSTEM_USER;

CREATE TRIGGER updated_publishers_TR ON dbo.Publishers
AFTER UPDATE
AS
UPDATE Publishers
SET updated =GETDATE(), updated_by = SYSTEM_USER;

drop trigger updated_publishers_TR 
go

CREATE TRIGGER updated_booksauthors_TR ON dbo.BooksAuthors
AFTER UPDATE
AS
UPDATE BooksAuthors
SET updated =GETDATE(), updated_by = SYSTEM_USER;

CREATE TRIGGER updated_authors_TR ON dbo.Authors
AFTER UPDATE
AS
UPDATE Authors
SET updated =GETDATE(), updated_by = SYSTEM_USER
INSERT INTO Authors_log
(author_id_new,name_new,[url_new],author_id_old,name_old,url_old,operation_type, operation_datetime)
SELECT inserted.id,inserted.name,inserted.url,deleted.id,deleted.name,deleted.url,'U',getdate()
from inserted
JOIN deleted ON inserted.inserted = deleted.inserted



CREATE TRIGGER insert_authors_TR ON dbo.Authors
AFTER INSERT
AS
INSERT INTO Authors_log
(author_id_new,name_new,[url_new],operation_type, operation_datetime)
SELECT id,[name],[url],'I',getdate()
from inserted

CREATE TRIGGER delete_authors_TR ON dbo.Authors
AFTER DELETE
AS
INSERT INTO Authors_log
(author_id_old,name_old,[url_old],operation_type, operation_datetime)
SELECT id,[name],[url],'D',getdate()
from deleted
