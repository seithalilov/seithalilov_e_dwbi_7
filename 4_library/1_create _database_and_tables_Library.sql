USE [master]
GO

CREATE DATABASE [E_S_Library] ON
PRIMARY
(NAME = 'Main', FILENAME = 'D:\DB\Main.mdf'),

FILEGROUP [Data]
(NAME = 'Data', FILENAME = 'D:\DB\Edem.ndf')
GO

USE E_S_Library
GO

ALTER DATABASE E_S_Library
   MODIFY FILEGROUP [DATA] DEFAULT;
GO

CREATE TABLE [Authors](
	id                 INT               not null,
    [name]             VARCHAR(30)       NOT NULL UNIQUE,
    [url]			   VARCHAR(30)		 NOT NULL,
    inserted		   DATETIME			 NOT NULL DEFAULT GETDATE(),
    inserted_by		   varchar(20)		 not null,
	updated 		   DATETIME			 NULL,
    updated_by		   varchar(20)		 null,
    PRIMARY KEY (id)
)

CREATE TABLE [Publishers](
	id                 INT               IDENTITY(1,1),
    [name]             VARCHAR(30)       NOT NULL,
    [url]			   VARCHAR(30)		 NOT NULL,
    inserted		   DATETIME			 NOT NULL default getdate(),
    inserted_by		   varchar(20)		 not null,
	updated 		   DATETIME			 NULL,
    updated_by		   varchar(20)		 null,
    PRIMARY KEY (id)
)

CREATE TABLE [Books](
	isbn               VARCHAR(30)       not null,
    publisher_id       int			     NOT NULL,
	[url]			   VARCHAR(30)       not null,
	price			   decimal(10,2)	 not null CHECK (price>=0) DEFAULT 0,
	inserted		   DATETIME			 NOT NULL DEFAULT GETDATE(),
    inserted_by		   varchar(20)		 not null,
	updated 		   DATETIME			 NULL,
    updated_by		   varchar(20)		 null,
	PRIMARY KEY(isbn),
	CONSTRAINT publisher_id_FK FOREIGN KEY (publisher_id) REFERENCES Publishers(id)
	ON DELETE NO ACTION
	ON UPDATE CASCADE
)

CREATE TABLE [BooksAuthors](
	id                 INT             not null,
    isbn             VARCHAR(30)       NOT NULL,
	author_id		 INT			   not null,
	seq_no			 INT			   not null DEFAULT 1 CHECK (seq_no>=1),
	inserted		   DATETIME			 NOT NULL,
    inserted_by		   varchar(20)		 not null,
	updated 		   DATETIME			 NULL,
    updated_by		   varchar(20)		 null,
	PRIMARY KEY(id),
	CONSTRAINT books_isbn_FK FOREIGN KEY (isbn) REFERENCES Books(isbn)
	ON DELETE NO ACTION
	ON UPDATE CASCADE,
	CONSTRAINT authors_id_FK FOREIGN KEY (author_id) REFERENCES Authors(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	UNIQUE (isbn,author_id)
)

CREATE TABLE [Authors_log](
	operation_id                 INT               IDENTITY(1,1),
    author_id_new           VARCHAR(30)       NULL,
    name_new		   VARCHAR(30)			  NULL,
	[url_new]			   VARCHAR(30)		  NULL,
	author_id_old           VARCHAR(30)       NULL,
    name_old		   VARCHAR(30)			  NULL,
    [url_old]			   VARCHAR(30)		  NULL,
	operation_type		varchar(1)			not null check (operation_type IN('U','D','I')),
	operation_datetime	datetime			not null default getdate(),
    PRIMARY KEY (operation_id)
)
