INSERT INTO Authors 
(			id,
			[name],
			[url]
)
VALUES		
(1,'Pushkin','www.pushkin.com'),
(2,'Dostoyevskiy','www.dostoyevskiy.com'),
(3,'London','www.j_london.com'),
(4,'Twen','www.mark_twen.com'),
(5,'Martin','www.martin.com'),
(6,'Esenin','www.esenin.com'),
(7,'Shevchenko','www.shevchenko.com'),
(8,'Gogol','www.gogol.com'),
(9,'Melnik','www.melnik.com'),
(10,'Tolstoy','www.tolstoy.com'),
(11,'Chekhov','www.chekhov.com'),
(12,'Dikkens','www.dikkens.com'),
(13,'Kafka','www.kafka.com'),
(14,'Lermontov','www.lermontov.com'),
(15,'King','www.king.com'),
(16,'J Joys','www.j_joys.com'),
(17,'Konan Doyl','www.kdoyl.com'),
(18,'Kristi','www.agata_kristi.com'),
(19,'Gorkiy','www.gorkiy.com'),
(20,'Remark','www.emremark.com');

INSERT INTO Publishers 
(
			[name],
			[url]
)
VALUES		
('Pushkin_pub','www.pushkin.com'),
('Dostoyevskiy_pub','www.dostoyevskiy.com'),
('London_pub','www.j_london.com'),
('Twen_pub','www.mark_twen.com'),
('Martin_pub','www.martin.com'),
('Esenin_pub','www.esenin.com'),
('Shevchenko_pub','www.shevchenko.com'),
('Gogol_pub','www.gogol.com'),
('Melnik_pub','www.melnik.com'),
('Tolstoy_pub','www.tolstoy.com'),
('Chekhov_pub','www.chekhov.com'),
('Dikkens_pub','www.dikkens.com'),
('Kafka_pub','www.kafka.com'),
('Lermontov_pub','www.lermontov.com'),
('King_pub','www.king.com'),
('J Joys_pub','www.j_joys.com'),
('Konan Doyl_pub','www.kdoyl.com'),
('Kristi_pub','www.agata_kristi.com'),
('Gorkiy_pub','www.gorkiy.com'),
('Remark_pub','www.emremark.com');

INSERT INTO Books 
(			isbn,
			publisher_id,
			[url],
			price
)
VALUES		
(1234567890,1,'www.pushkin.com/1',154.4),
(1234563243,2,'www.dostoyevskiy.com/2',160),
(4234234340,1,'www.pushkin.com/2',254.4),
(2345243534,2,'www.dostoyevskiy.com/3',299.4),
(5346573240,5,'www.books.com/2',233.4),
(2352352344,11,'www.ebook.com/3',77.8),
(2344545546,5,'www.ebook.com/4',234),
(6876463434,8,'www.library.com/5',353),
(7353985345,6,'www.library.com/6',54),
(0239403825,14,'www.library.com/7',234),
(4258943589,4,'www.ebook.com/88',233),
(1235464334,3,'www.books.com/9',434),
(3453455540,7,'www.library.com/10',198),
(3453453453,15,'www.library.com/11',203),
(3563552233,12,'www.books.com/12',34),
(6553255644,14,'www.library.com/23',153),
(9875622145,17,'www.books.com/343',174),
(8971926982,7,'www.ebook.com/353',166),
(9879283378,6,'www.ebook.com/2352',235),
(9874651232,4,'www.ebook.com/232',135);

INSERT INTO BooksAuthors 
(			id,
			isbn,
			author_id,
			seq_no
)
VALUES		
(1,1234567890,1,1),
(2,1234563243,2,2),
(3,4234234340,1,3),
(4,2345243534,2,4),
(5,5346573240,5,5),
(6,2352352344,11,6),
(7,2344545546,5,7),
(8,6876463434,8,default),
(9,7353985345,6,2),
(10,0239403825,14,4),
(11,4258943589,4,5),
(12,1235464334,3,2),
(13,3453455540,7,3),
(14,3453453453,15,5),
(15,3563552233,12,2),
(16,6553255644,14,3),
(17,9875622145,17,5),
(18,8971926982,7,2),
(19,9879283378,6,3),
(20,9874651232,4,5);

SELECT * FROM Authors
SELECT * FROM Authors_log
SELECT * FROM Books
SELECT * FROM BooksAuthors
SELECT * FROM Publishers

DELETE Authors_log
WHERE operation_id = 1

UPDATE Authors
SET [url] = 'www.pushkin_alex.com' 
WHERE name = 'Pushkin'

UPDATE Authors
SET [url] = 'www.authors.com/' + convert(varchar(10),id)
WHERE id BETWEEN 1 and 10


UPDATE Books
SET price = price - 0.01 


UPDATE BooksAuthors
SET seq_no = 5
WHERE id between 10 and 20


UPDATE Publishers
SET name = 'Mr ' + name 
WHERE id between 1 and 10

DELETE TOP (5)
FROM BooksAuthors

DELETE Books
WHERE isbn NOT IN (SELECT isbn from BooksAuthors)

DELETE Authors
WHERE id NOT IN (select author_id from BooksAuthors)

DELETE Publishers
WHERE id NOT IN (select publisher_id from Books)